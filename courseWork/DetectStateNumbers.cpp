//! \brief DetectStateNumbers.cpp

#include "DetectStateNumbers.h"


std::vector<PossibleStateNumber> detectStateNumberOnImage(cv::Mat &imgOriginal, int & number_photo) {
    std::vector<PossibleStateNumber> vectorOfPossibleStateNumbers;

    cv::Mat imgGrayscale;
    cv::Mat imgThresh;

    cv::destroyAllWindows();

    preprocess(imgOriginal, imgGrayscale, imgThresh);        // ���������� �����������

    std::vector<PossibleChar> vectorOfPossibleCharsOnImage = findPossibleCharsOnImage(imgThresh);

    std::vector<std::vector<PossibleChar> > vectorOfVectorsOfMatchingCharsOnImage = findVectorOfVectorsOfMatchingChars(vectorOfPossibleCharsOnImage);

    cv::Mat imgContours(imgOriginal.size(), CV_8UC3, SCALAR_BLACK);
    imgContours = cv::Mat(imgOriginal.size(), CV_8UC3, SCALAR_BLACK);

    for (auto &vectorOfMatchingChars : vectorOfVectorsOfMatchingCharsOnImage) {                     // ��� ���� ����� �������� ����� �������� ��������� �������������
        PossibleStateNumber possibleStateNumber = extractStateNumber(imgOriginal, vectorOfMatchingChars);        

        if (possibleStateNumber.imgStateNumber.empty() == false) {                                              // ���� ������������� ������� ���������, �� ��������� ��� � ������
            vectorOfPossibleStateNumbers.push_back(possibleStateNumber);                                       
        }
    }

    std::cout << std::endl << vectorOfPossibleStateNumbers.size() << " possible StateNumbers found" << std::endl;

    cv::Point2f p2fRectPoints[4];
    cv::Point vertices[4];

    vectorOfPossibleStateNumbers[0].RotatedRectLocatioin.points(p2fRectPoints);

    for (int j = 0; j < 4; j++) {
        //cv::line(imgContours, p2fRectPoints[j], p2fRectPoints[(j + 1) % 4], SCALAR_RED, 2);
        vertices[j] = p2fRectPoints[j];
    }

    cv::fillConvexPoly(imgContours,vertices,4,SCALAR_WHITE);
    cv::imwrite("Images_Output\\image_output_mask" + std::to_string(number_photo) + ".jpg", imgContours);

    return vectorOfPossibleStateNumbers;
}


std::vector<PossibleChar> findPossibleCharsOnImage(cv::Mat &imgThresh) {
    std::vector<PossibleChar> vectorOfPossibleChars; 

    cv::Mat imgContours(imgThresh.size(), CV_8UC3, SCALAR_BLACK);
    int CountOfPossibleChars = 0;

    cv::Mat imgThreshCopy = imgThresh.clone();

    std::vector<std::vector<cv::Point> > contours;

    cv::findContours(imgThreshCopy, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);

    for (unsigned int i = 0; i < contours.size(); i++) {   

        PossibleChar possibleChar(contours[i]);

        if (checkIfPossibleChar(possibleChar)) {                // �������� �� ����������� ������� ������� � �������
            CountOfPossibleChars++;                          
            vectorOfPossibleChars.push_back(possibleChar);     
        }
    }

    return(vectorOfPossibleChars);
}

PossibleStateNumber extractStateNumber(cv::Mat &imgOriginal, std::vector<PossibleChar> &vectorOfMatchingChars) {
    PossibleStateNumber possibleStateNumber;           

    std::sort(vectorOfMatchingChars.begin(), vectorOfMatchingChars.end(), PossibleChar::sortChars);

    // ���������� ������ ��������������
    double stateNumberCenterX = (double)(vectorOfMatchingChars[0].centerX + vectorOfMatchingChars[vectorOfMatchingChars.size() - 1].centerX) / 2.0;
    double stateNumberCenterY = (double)(vectorOfMatchingChars[0].centerY + vectorOfMatchingChars[vectorOfMatchingChars.size() - 1].centerY) / 2.0;
    cv::Point2d p2dStateNumberCenter(stateNumberCenterX, stateNumberCenterY);

    // ���������� ������ � ������ ��������������
    int stateNumberWidth = (int)((vectorOfMatchingChars[vectorOfMatchingChars.size() - 1].boundingRect.x + vectorOfMatchingChars[vectorOfMatchingChars.size() - 1].boundingRect.width - vectorOfMatchingChars[0].boundingRect.x) * STATE_NUMBER_WIDTH_PADDING_FACTOR);

    double totalOfCharHeights = 0;

    for (auto &matchingChar : vectorOfMatchingChars) {
        totalOfCharHeights = totalOfCharHeights + matchingChar.boundingRect.height;
    }

    double averageCharHeight = (double)totalOfCharHeights / vectorOfMatchingChars.size();

    int stateNumberHeight = (int)(averageCharHeight * STATE_NUMBER_HEIGHT_PADDING_FACTOR);

    double opposite = vectorOfMatchingChars[vectorOfMatchingChars.size() - 1].centerY - vectorOfMatchingChars[0].centerY;
    double hypotenuse = distanceBetweenChars(vectorOfMatchingChars[0], vectorOfMatchingChars[vectorOfMatchingChars.size() - 1]);
    double correctionAngleInRad = asin(opposite / hypotenuse);
    double correctionAngleInDeg = correctionAngleInRad * (180.0 / CV_PI);

    possibleStateNumber.RotatedRectLocatioin = cv::RotatedRect(p2dStateNumberCenter, cv::Size2f((float)stateNumberWidth, (float)stateNumberHeight), (float)correctionAngleInDeg);

    cv::Mat rotationMatrix;           
    cv::Mat imgRotated;
    cv::Mat imgCropped;

    rotationMatrix = cv::getRotationMatrix2D(p2dStateNumberCenter, correctionAngleInDeg, 1.0);         // �������� ������� �������� �� ����, ������� ��������� ����

    cv::warpAffine(imgOriginal, imgRotated, rotationMatrix, imgOriginal.size());            // ��� ������� �����������

    cv::getRectSubPix(imgRotated, possibleStateNumber.RotatedRectLocatioin.size, possibleStateNumber.RotatedRectLocatioin.center, imgCropped);

    possibleStateNumber.imgStateNumber = imgCropped;

    return(possibleStateNumber);
}

std::vector<PossibleStateNumber> detectCharsInStateNumbers(std::vector<PossibleStateNumber> &vectorOfPossibleStateNumbers) {

    if (vectorOfPossibleStateNumbers.empty()) {               
        return(vectorOfPossibleStateNumbers);                 
    }

    for (auto &possibleStateNumber : vectorOfPossibleStateNumbers) {            // ��� ������� ���������� �������������� ��� ������� ���� for, ������� �������� ������� ����� �������

        preprocess(possibleStateNumber.imgStateNumber, possibleStateNumber.imgGrayscale, possibleStateNumber.imgThresh);        // ���������� ����������� ��� ��������� grayscale � threshold �����������

        // ����������� ������ �� 60% ��� ������� ��������� � ������������� ��������
        cv::resize(possibleStateNumber.imgThresh, possibleStateNumber.imgThresh, cv::Size(), 1.6, 1.6);

        // ����� �����, ����� ��������� ����� ����� �������
        cv::threshold(possibleStateNumber.imgThresh, possibleStateNumber.imgThresh, 0.0, 255.0, CV_THRESH_BINARY | CV_THRESH_OTSU);

        // ��� ������� ������� ������� ��� �������, � ����� �������� ������ �������, ������� ����� ���� ��������� (��� ��������� � ������� ���������)
        std::vector<PossibleChar> vectorOfPossibleCharsInStateNumber = findPossibleCharsInStateNumbers(possibleStateNumber.imgGrayscale, possibleStateNumber.imgThresh);

        // �������� ������ ���� ��������� ��������, ���� ������ ����������� �������� ������ ��������������
        std::vector<std::vector<PossibleChar> > vectorOfVectorsOfMatchingCharsInStateNumber = findVectorOfVectorsOfMatchingChars(vectorOfPossibleCharsInStateNumber);

        for (auto &vectorOfMatchingChars : vectorOfVectorsOfMatchingCharsInStateNumber) {                                        
            std::sort(vectorOfMatchingChars.begin(), vectorOfMatchingChars.end(), PossibleChar::sortChars);      
            vectorOfMatchingChars = removeInternalOverlappingChars(vectorOfMatchingChars);                                     
        }


        // � �������� ������� ���������� �������������� �����������, ��� ����� ������� ������ ������������� ����������� �������� �������� ����������� �������� ��������
        unsigned int intLenOfLongestVectorOfChars = 0;
        unsigned int intIndexOfLongestVectorOfChars = 0;
        // ��������� ��� ������� ����������� ��������, �������� ������ ����, � �������� ������ ����� ��������
        for (unsigned int i = 0; i < vectorOfVectorsOfMatchingCharsInStateNumber.size(); i++) {
            if (vectorOfVectorsOfMatchingCharsInStateNumber[i].size() > intLenOfLongestVectorOfChars) {
                intLenOfLongestVectorOfChars = vectorOfVectorsOfMatchingCharsInStateNumber[i].size();
                intIndexOfLongestVectorOfChars = i;
            }
        }

        // �����������, ��� ����� ������� ������ ����������� �������� ������ �������������� �������� ����������� �������� ��������
        std::vector<PossibleChar> longestVectorOfMatchingCharsInStateNumber = vectorOfVectorsOfMatchingCharsInStateNumber[intIndexOfLongestVectorOfChars];


    }  



    return(vectorOfPossibleStateNumbers);
}

std::vector<PossibleChar> findPossibleCharsInStateNumbers(cv::Mat &imgGrayscale, cv::Mat &imgThresh) {
    std::vector<PossibleChar> vectorOfPossibleChars;

    cv::Mat imgThreshCopy;

    std::vector<std::vector<cv::Point> > contours;

    imgThreshCopy = imgThresh.clone();

    cv::findContours(imgThreshCopy, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);        // ����� �������� � ��������������

    for (auto &contour : contours) {                            
        PossibleChar possibleChar(contour);

        if (checkIfPossibleChar(possibleChar)) {                // ���� ������ �������� ��������� ��������
            vectorOfPossibleChars.push_back(possibleChar);      
        }
    }

    return(vectorOfPossibleChars);
}

bool checkIfPossibleChar(PossibleChar &possibleChar) {
    if (possibleChar.boundingRect.area() > MIN_PIXEL_AREA &&
        possibleChar.boundingRect.width > MIN_PIXEL_WIDTH && 
        possibleChar.boundingRect.height > MIN_PIXEL_HEIGHT &&
        MIN_ASPECT_RATIO < possibleChar.aspectRatio && possibleChar.aspectRatio < MAX_ASPECT_RATIO) 
    {
        return(true);
    }
    else {
        return(false);
    }
}


std::vector<std::vector<PossibleChar> > findVectorOfVectorsOfMatchingChars(const std::vector<PossibleChar> &vectorOfPossibleChars) {
    std::vector<std::vector<PossibleChar> > vectorOfVectorsOfMatchingChars;     

    for (auto &possibleChar : vectorOfPossibleChars) {                  // ��� ������� ���������� ������� � ����� ������� ������� ��������

        std::vector<PossibleChar> vectorOfMatchingChars = findVectorOfMatchingChars(possibleChar, vectorOfPossibleChars);

        vectorOfMatchingChars.push_back(possibleChar);
        if (vectorOfMatchingChars.size() < MIN_NUMBER_OF_MATCHING_CHARS) {
            continue;                      
        }
       
        vectorOfVectorsOfMatchingChars.push_back(vectorOfMatchingChars);

        //������� ������ ������, ����� �� ������������ ��� ������
        std::vector<PossibleChar> vectorOfPossibleCharsWithCurrentMatchesRemoved;

        for (auto &possChar : vectorOfPossibleChars) {
            if (std::find(vectorOfMatchingChars.begin(), vectorOfMatchingChars.end(), possChar) == vectorOfMatchingChars.end()) {
                vectorOfPossibleCharsWithCurrentMatchesRemoved.push_back(possChar);
            }
        }
        // �������� ����� ������ �������� ��������, ������� ������� ����� ��������
        std::vector<std::vector<PossibleChar> > recursiveVectorOfVectorsOfMatchingChars;

        //���� ��������
        recursiveVectorOfVectorsOfMatchingChars = findVectorOfVectorsOfMatchingChars(vectorOfPossibleCharsWithCurrentMatchesRemoved);

        for (auto &recursiveVectorOfMatchingChars : recursiveVectorOfVectorsOfMatchingChars) {      
            vectorOfVectorsOfMatchingChars.push_back(recursiveVectorOfMatchingChars);               
        }

        break;
    }

    return(vectorOfVectorsOfMatchingChars);
}


std::vector<PossibleChar> findVectorOfMatchingChars(const PossibleChar &possibleChar, const std::vector<PossibleChar> &vectorOfChars) {
    std::vector<PossibleChar> vectorOfMatchingChars;                

    for (auto &possibleMatchingChar : vectorOfChars) {              

                                                                    
        if (possibleMatchingChar == possibleChar) {
            continue;           //����� ������ �� ��������� ���� � ��� �� ������ � ������, ���������� ���
        }
        
        double distanceBetweenChars_ = distanceBetweenChars(possibleChar, possibleMatchingChar);
        double angleBetweenChars_ = angleBetweenChars(possibleChar, possibleMatchingChar);
        double changeInArea_ = (double)abs(possibleMatchingChar.boundingRect.area() - possibleChar.boundingRect.area()) / (double)possibleChar.boundingRect.area();
        double changeInWidth = (double)abs(possibleMatchingChar.boundingRect.width - possibleChar.boundingRect.width) / (double)possibleChar.boundingRect.width;
        double changeInHeight = (double)abs(possibleMatchingChar.boundingRect.height - possibleChar.boundingRect.height) / (double)possibleChar.boundingRect.height;

        //�������� ������� �� ����������
        if (distanceBetweenChars_ < (possibleChar.diagonalSize * MAX_DIAG_SIZE_MULTIPLE_AWAY) &&
            angleBetweenChars_ < MAX_ANGLE_BETWEEN_CHARS &&
            changeInArea_ < MAX_CHANGE_IN_AREA &&
            changeInWidth < MAX_CHANGE_IN_WIDTH &&
            changeInHeight < MAX_CHANGE_IN_HEIGHT) {
            vectorOfMatchingChars.push_back(possibleMatchingChar);
        }
    }

    return(vectorOfMatchingChars);
}

//������ ���������� ����� ����� ��������� �� ������� ��������
double distanceBetweenChars(const PossibleChar &firstChar, const PossibleChar &secondChar) {
    int intX = abs(firstChar.centerX - secondChar.centerX);
    int intY = abs(firstChar.centerY - secondChar.centerY);

    return(sqrt(pow(intX, 2) + pow(intY, 2)));
}

//���� ����� ����� ��������� ������ ����� ����������
double angleBetweenChars(const PossibleChar &firstChar, const PossibleChar &secondChar) {
    double adjacent = abs(firstChar.centerX - secondChar.centerX);
    double opposite = abs(firstChar.centerY - secondChar.centerY);

    double angleInRad = atan(opposite / adjacent);

    double angleInDeg = angleInRad * (180.0 / CV_PI);

    return(angleInDeg);
}

//���� ���� ��� �������, ������������� ���� ����� ��� ����������� ������� ������, �� ������� ������� �� ���
std::vector<PossibleChar> removeInternalOverlappingChars(std::vector<PossibleChar> &vectorOfMatchingChars) {
    std::vector<PossibleChar> vectorOfMatchingCharsWithInnerCharRemoved(vectorOfMatchingChars);

    for (auto &currentChar : vectorOfMatchingChars) {
        for (auto &otherChar : vectorOfMatchingChars) {
            if (currentChar != otherChar) {                         
                if (distanceBetweenChars(currentChar, otherChar) < (currentChar.diagonalSize * MIN_DIAG_SIZE_MULTIPLE_AWAY)) {
                    //�������� �� ���������������

                    // ���� ������� �� ������� �� ���
                    if (currentChar.boundingRect.area() < otherChar.boundingRect.area()) {
                        std::vector<PossibleChar>::iterator currentCharIterator = std::find(vectorOfMatchingCharsWithInnerCharRemoved.begin(), vectorOfMatchingCharsWithInnerCharRemoved.end(), currentChar);
                        if (currentCharIterator != vectorOfMatchingCharsWithInnerCharRemoved.end()) {
                            vectorOfMatchingCharsWithInnerCharRemoved.erase(currentCharIterator);
                        }
                    }
                    else {
                        std::vector<PossibleChar>::iterator otherCharIterator = std::find(vectorOfMatchingCharsWithInnerCharRemoved.begin(), vectorOfMatchingCharsWithInnerCharRemoved.end(), otherChar);
                        if (otherCharIterator != vectorOfMatchingCharsWithInnerCharRemoved.end()) {
                            vectorOfMatchingCharsWithInnerCharRemoved.erase(otherCharIterator);
                        }
                    }
                }
            }
        }
    }

    return(vectorOfMatchingCharsWithInnerCharRemoved);
}

