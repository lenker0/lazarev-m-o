//! \brief DetectStateNumbers.h

#ifndef DETECT_StateNumbers_H
#define DETECT_StateNumbers_H

#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>

#include "Main.h"
#include "PossibleStateNumber.h"
#include "PossibleChar.h"
#include "Preprocess.h"

//! \brief  ����������� ������ � ��������
const int MIN_PIXEL_WIDTH = 2;

//! \brief  ����������� ������ � ��������
const int MIN_PIXEL_HEIGHT = 8;

//! \brief  ����������� ����������� ������
const double MIN_ASPECT_RATIO = 0.25;

//! \brief  ������������ ����������� ������
const double MAX_ASPECT_RATIO = 1.0;

//! \brief  ����������� ������� � ��������
const int MIN_PIXEL_AREA = 80;

//��� ��������� ���� ��������

//! \brief  ����������� ����������� ��������� ��� ���������
const double MIN_DIAG_SIZE_MULTIPLE_AWAY = 0.3;

//! \brief ������������ ����������� ��������� ��� ���������
const double MAX_DIAG_SIZE_MULTIPLE_AWAY = 5.0;

//! \brief  ������������ ������� � �������
const double MAX_CHANGE_IN_AREA = 0.5;

//! \brief  ������������ ������� �����
const double MAX_CHANGE_IN_WIDTH = 0.8;

//! \brief  ������������ ������� �����
const double MAX_CHANGE_IN_HEIGHT = 0.2;

//! \brief ������������ ���� ����� ���������
const double MAX_ANGLE_BETWEEN_CHARS = 12.0;

//! \brief  ����������� ���������� ����������� ��������
const int MIN_NUMBER_OF_MATCHING_CHARS = 3;

//! \brief  ����������� ���������� ������ ��������� ����� 
const double STATE_NUMBER_WIDTH_PADDING_FACTOR = 1.3;
//! \brief  ����������� ���������� ������ ��������� ����� 
const double STATE_NUMBER_HEIGHT_PADDING_FACTOR = 1.5;

//! \brief ����������� ��������������� � �����������
std::vector<PossibleStateNumber> detectStateNumberOnImage(cv::Mat &imgOriginal, int &number_photo);

//! \brief ����������� ��������� �������� � �����������
std::vector<PossibleChar> findPossibleCharsOnImage(cv::Mat &imgThresh);

//! \brief ����������� �������������� �� ����������� ��������
PossibleStateNumber extractStateNumber(cv::Mat &imgOriginal, std::vector<PossibleChar> &vectorOfMatchingChars);

//! \brief  ����������� �������� � ���������������
std::vector<PossibleStateNumber> detectCharsInStateNumbers(std::vector<PossibleStateNumber>& vectorOfPossibleStateNumbers);

//! \brief ����� ��������� �������� �� ��������
std::vector<PossibleChar> findPossibleCharsInStateNumbers(cv::Mat& imgGrayscale, cv::Mat& imgThresh);

//! \brief �������� �� ������ ��������
bool checkIfPossibleChar(PossibleChar& possibleChar);

//! \brief ����� ����� ����������� �������� ������ ��������������
std::vector<std::vector<PossibleChar> > findVectorOfVectorsOfMatchingChars(const std::vector<PossibleChar>& vectorOfPossibleChars);

//! \brief ���� ����������� �������
std::vector<PossibleChar> findVectorOfMatchingChars(const PossibleChar& possibleChar, const std::vector<PossibleChar>& vectorOfChars);

//! \brief ���������� ����� ���������
double distanceBetweenChars(const PossibleChar& firstChar, const PossibleChar& secondChar);

//! \brief ���� ����� ���������
double angleBetweenChars(const PossibleChar& firstChar, const PossibleChar& secondChar);

//! \brief ����������� ��������������� �������� � �������� �������� �� ���
std::vector<PossibleChar> removeInternalOverlappingChars(std::vector<PossibleChar>& vectorOfMatchingChars);

# endif	// DETECT_StateNumbers_H

