//! \brief Main.cpp

#include "Main.h"
#include "windows.h"

int main(void) {

    cv::RotatedRect mask_ideal = cv::RotatedRect(cv::Point2f(402, 232), cv::Size2f(108, 25), 1.7);
    cv::RotatedRect mask_ideal7 = cv::RotatedRect(cv::Point2f(256, 194), cv::Size2f(76, 20), 0);

    for (int i = 1; i < 9; i++)
    {
        cv::Mat imgOriginal;

        /*WIN32_FIND_DATA data;
        HANDLE hFind = FindFirstFile("D:\\Study\\Cpp Projects\\lazarev_m_o\\build\\prj.labs\\lab01\\Images\\*.jpg", &data);
        std::cout << "List of images, for analyzing numbers: \n";


        if (hFind != INVALID_HANDLE_VALUE) {
            do {
                std::cout << data.cFileName << std::endl;
            } while (FindNextFile(hFind, &data));
            FindClose(hFind);
        }

        std::string inputName;
        std::cout << "Enter the image name without its extension: ";
        std::cin >> inputName;*/
        imgOriginal = cv::imread("Images_Input\\image" + std::to_string(i) + ".jpg");        

        if (imgOriginal.empty()) {                             // ���� �� ������� ������� �����������
            std::cout << "error: the image is not being read from the file";     // �������� ��������� �� ������ � ��������� ������
            return(0);                                              // � ����� �� ���������
        }

        std::vector<PossibleStateNumber> vectorOfPossibleStateNumbers = detectStateNumberOnImage(imgOriginal, i);          // ����������� ��������������� � �������

        vectorOfPossibleStateNumbers = detectCharsInStateNumbers(vectorOfPossibleStateNumbers);                               // ����������� �������� � ���������������

        if (vectorOfPossibleStateNumbers.empty()) {
            std::cout << std::endl << "StateNumbers were not found" << std::endl;
        }
        else {

            // �����������, ��� �������������, ��������� ������, �������� ������� �������

            //cv::imshow("�������������� �����", vectorOfPossibleStateNumbers[0].imgStateNumber);
            cv::imwrite("Images_Output\\image_output" + std::to_string(i) + ".jpg", vectorOfPossibleStateNumbers[0].imgStateNumber);
            drawRectOnImage(imgOriginal, vectorOfPossibleStateNumbers[0]);

            //cv::imshow("��������", imgOriginal);

          
            float IOU = 0;

            if (i == 1) {
                IOU = rotatedRectIOU(vectorOfPossibleStateNumbers[0].RotatedRectLocatioin, mask_ideal);
                
            }
            if (i == 7) {
                IOU = rotatedRectIOU(vectorOfPossibleStateNumbers[0].RotatedRectLocatioin, mask_ideal7);

            }

            std::cout << "IOU image" + std::to_string(i) + " = " << IOU;
        }

        cv::waitKey(0);
    }
    

    return(0);
}

//��������� ������� ������ ���������� ������
void drawRectOnImage(cv::Mat &imgOriginal, PossibleStateNumber &licStateNumber) {
    cv::Point2f p2fRectPoints[4];

    licStateNumber.RotatedRectLocatioin.points(p2fRectPoints);           

    for (int i = 0; i < 4; i++) {                                      
        cv::line(imgOriginal, p2fRectPoints[i], p2fRectPoints[(i + 1) % 4], SCALAR_RED, 2);
    }
}

float rotatedRectIOU(const cv::RotatedRect& a, const cv::RotatedRect& b)
{
    std::vector<cv::Point2f> inter;
    int res = rotatedRectangleIntersection(a, b, inter);
    if (inter.empty() || res == cv::INTERSECT_NONE)
        return 0.0f;
    if (res == cv::INTERSECT_FULL)
        return 1.0f;
    float interArea = cv::contourArea(inter);
    return interArea / (a.size.area() + b.size.area() - interArea);
}
