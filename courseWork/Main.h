//! \brief Main.h

#ifndef MY_MAIN        
#define MY_MAIN

#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgproc/imgproc_c.h>
#include <opencv2/core_detect.hpp>

#include "DetectStateNumbers.h"
#include "PossibleStateNumber.h"

#include<iostream>
#include<conio.h>        

//! \brief ������ ���� 
const cv::Scalar SCALAR_BLACK = cv::Scalar(0.0, 0.0, 0.0);
//! \brief ����� ���� 
const cv::Scalar SCALAR_WHITE = cv::Scalar(255.0, 255.0, 255.0);
//! \brief ������� ���� 
const cv::Scalar SCALAR_RED = cv::Scalar(0.0, 0.0, 255.0);

//! \brief main
int main(void);

//! \brief ��������� ������� ����� ������ �������� ������
void drawRectOnImage(cv::Mat &imgOriginal, PossibleStateNumber &licStateNumber);

//! \brief ��������� IOU
float rotatedRectIOU(const cv::RotatedRect& a, const cv::RotatedRect& b);


# endif

