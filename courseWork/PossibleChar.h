//! \brief PossibleChar.h

#ifndef POSSIBLE_CHAR_H
#define POSSIBLE_CHAR_H

#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>

//! \brief  ��������� ������
class PossibleChar {
public:
    //! \brief  ������
    std::vector<cv::Point> contour;

    //! \brief  �������������� �������������
    cv::Rect boundingRect;

    //! \brief  ����� �� ���������� X
    int centerX;
    //! \brief  ����� �� ���������� Y
    int centerY;
    //! \brief  ���������
    double diagonalSize;
    //! \brief  ����������� ������
    double aspectRatio;

    //! \brief ���������� �������� �� ���������� � (����� �������)
    static bool sortChars(const PossibleChar &pcLeft, const PossibleChar & pcRight) {
        return(pcLeft.centerX < pcRight.centerX);
    }

    //! \brief ���������� ��������� 
    bool operator == (const PossibleChar& otherPossibleChar) const {
        if (this->contour == otherPossibleChar.contour) return true;
        else return false;
    }

    //! \brief ���������� ���������  
    bool operator != (const PossibleChar& otherPossibleChar) const {
        if (this->contour != otherPossibleChar.contour) return true;
        else return false;
    }

    //! \brief ������������� ������
    PossibleChar(std::vector<cv::Point> _contour);

};

#endif  // POSSIBLE_CHAR_H

