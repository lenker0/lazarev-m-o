//! \brief PossibleStateNumber.h

#ifndef POSSIBLE_StateNumber_H
#define POSSIBLE_StateNumber_H

#include <string>

#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>

//! \brief  ��������� ���.�����
class PossibleStateNumber {
public:
    //! \brief ����������� ������
    cv::Mat imgStateNumber;
    
    //! \brief  ����������� Grayscal'a
    cv::Mat imgGrayscale;

    //! \brief  ����������� threshold'a
    cv::Mat imgThresh;

    //! \brief ���������� ����������� 
    cv::RotatedRect RotatedRectLocatioin;

};


#endif	

