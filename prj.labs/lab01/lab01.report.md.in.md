Работа 1. Исследование гамма-коррекции
--------------------------------------

автор: Лазарев М.О. дата: 24.02.2021

<!-- url: https://gitlab.com/lenker0/lazarev-m-o -->

### Задание

1.  Сгенерировать серое тестовое изображение **I_1** в виде прямоугольника
    размером 768х60 пикселя с плавным изменение пикселей от черного к
    белому, одна градация серого занимает 3 пикселя по горизонтали.
2.  Применить к изображению **I_1** гамма-коррекцию с коэффициентом из
    интервала 2.2-2.4 и получить изображение **G_1** при помощи функци
    pow.
3.  Применить к изображению **I_1** гамма-коррекцию с коэффициентом из
    интервала 2.2-2.4 и получить изображение **G_2** при помощи прямого
    обращения к пикселям.
4.  Показать визуализацию результатов в виде одного изображения (сверху
    вниз **I_1**, **G_1**, **G_2**).
5.  Сделать замер времени обработки изображений в п.2 и п.3, результаты
    отфиксировать в отчете.

### Результаты

![](lab01.png)

Рис. 1. Результаты работы программы (сверху вниз **I_1**, **G_1**, **G_2**)

Время работы 2 пункта: 0.004000 секунд

Время работы 3 пункта: 0.006000 секунд

Следовательно, гамма-коррекция при помощи функци pow существенно быстрее, чем гамма-коррекция при помощи прямого обращения к пикселям.

### Текст программы

``` {.cpp}
#include <opencv2/opencv.hpp>
#include <cmath>

using namespace cv;
using namespace std;

Mat mat_gray_gradient() {
    Mat img(Mat::zeros(60, 768, 0));
    for (size_t i = 0; i <= 255; i += 1)
    {
        line(img, Point(i * 3, 0), Point(i * 3, 60), i, 3);
    }
    return img;
}

Mat gammacorr_with_pow(Mat img, double alpha) {
    clock_t start = clock();
    Mat img_temp, img_pow;

    img.convertTo(img_temp, CV_64F);
    img_temp /= 255;
    pow(img_temp, alpha, img_pow);
    img_pow *= 255;
    img_pow.convertTo(img_temp, 0);
    clock_t end = clock();
    double seconds = (double)(end - start) / CLOCKS_PER_SEC;
    printf("The time with pow: %f seconds\n", seconds);
    return img_temp;
}

Mat gammacorr_pixel(Mat img, double alpha) {
    clock_t start = clock();
    for (size_t i = 0; i < 60; i++)
    {

        for (size_t j = 0; j < 768; j++)
        {
            img.at<uchar>(i, j) = (pow(double(img.at<uchar>(i, j)) / 255, alpha) * 255);
        }
    }
    clock_t end = clock();
    double seconds = (double)(end - start) / CLOCKS_PER_SEC;
    printf("The time with direct access to pixels: %f seconds\n", seconds);
    return img;
}

Mat show_img(Mat source,Mat img_pow, Mat img_pixel) {
    Mat result(source.rows * 3, source.cols, 0);
    Mat Img1(result, Rect(0, 0, source.cols, source.rows));
    Mat Img2(result, Rect(0, source.rows, source.cols, source.rows));
    Mat Img3(result, Rect(0, source.rows * 2, source.cols, source.rows));
    source.copyTo(Img1);
    img_pow.copyTo(Img2);
    img_pixel.copyTo(Img3);
    return result;
}

int main()
{
    Mat I_1 = mat_gray_gradient();
    Mat G_1 = gammacorr_with_pow(I_1, 2.2);
    Mat G_2 = gammacorr_pixel(I_1, 2.2);
    Mat result = show_img(I_1, G_1, G_2);
    imshow("lab01", result);
    imwrite("lab01.png", result);
    waitKey(0);
    return 0;
}
```
