#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>


using namespace std;
using namespace cv;

void compress(Mat img, int image_quality) {
	vector<int> compression_params;
	compression_params.push_back(IMWRITE_JPEG_QUALITY);
	compression_params.push_back(image_quality);
	imwrite("Results_lab02/cross_0256x0256_025.jpg", img, compression_params);
}

void hist(Mat image, Mat imgG, Mat imgG25) {

	Mat imgConv = imread("Results_lab02/cross_0256x0256.jpg");

	cvtColor(image, imgG, COLOR_BGR2GRAY);
	cvtColor(imgConv, imgG25, COLOR_BGR2GRAY);

	Mat hist(256, 512, CV_8U, 255);
	Mat hist25(256, 512, CV_8U, 255);

	vector<int> vect(256);
	for (int i = 0; i < 256; i++)
		for (int j = 0; j < 256; j++)
			vect[imgG.at<uchar>(j, i)]++;
	int max = *max_element(vect.begin(), vect.end());
	for (int i = 0; i < 256; i++)
	{
		int t = vect[i] * 255 / max;
		Point p1(i * 2, 255 - t);
		Point p2(i * 2 + 1, 255);
		rectangle(hist, p1, p2, 0);
	}

	vector<int>vect_25(256);
	for (int i = 0; i < 256; i++)
		for (int j = 0; j < 256; j++)
			vect_25[imgG25.at<uchar>(j, i)]++;

	max = *max_element(vect_25.begin(), vect_25.end());
	for (int i = 0; i < 256; i++)
	{
		int t = vect_25[i] * 255 / max;
		Point p1(i * 2, 255 - t);
		Point p2(i * 2 + 1, 255);
		rectangle(hist25, p1, p2, 0);
	}

	Mat result(256, 256, CV_32F);
	hconcat(hist, hist25, result);
	imwrite("Results_lab02/cross_0256x0256_hists.png", result);
}

void BGRSplitAndMerge(Mat image, Mat imageJPG) {
	Mat imgB, imgG, imgR, imgB25, imgG25, imgR25;
	vector<Mat> channels;
	vector<Mat> ch(3);
	split(image, ch);
	Mat tmp = Mat::zeros(Size(image.cols, image.rows), CV_8UC1);

	channels.push_back(ch[0]);
	channels.push_back(tmp);
	channels.push_back(tmp);
	merge(channels, imgB);

	channels.clear();
	channels.push_back(tmp);
	channels.push_back(ch[1]);
	channels.push_back(tmp);
	merge(channels, imgG);

	channels.clear();
	channels.push_back(tmp);
	channels.push_back(tmp);
	channels.push_back(ch[2]);
	merge(channels, imgR);

	/*Clear channels and split for JPG img*/
	ch.clear();
	split(imageJPG, ch);
	/*--*/

	channels.clear();
	channels.push_back(ch[0]);
	channels.push_back(tmp);
	channels.push_back(tmp);
	merge(channels, imgB25);

	channels.clear();
	channels.push_back(tmp);
	channels.push_back(ch[1]);
	channels.push_back(tmp);
	merge(channels, imgG25);

	channels.clear();
	channels.push_back(tmp);
	channels.push_back(tmp);
	channels.push_back(ch[2]);
	merge(channels, imgR25);

	hconcat(image, imgR, image);
	hconcat(imgG, imgB, imgG);
	vconcat(image, imgG, image);

	hconcat(imageJPG, imgR25, imageJPG);
	hconcat(imgG25, imgB25, imgG25);
	vconcat(imageJPG, imgG25, imageJPG);

	imwrite("Results_lab02/cross_0256x0256_png_channels.png", image);
	imwrite("Results_lab02/cross_0256x0256_jpg_channels.png", imageJPG);

	hist(image, imgG, imgG25);
}

int main() {

	string path = "Resources/cross_0256x0256.png";
	Mat image = imread(path);
	Mat imageJPG;
	compress(image, 25);
	imageJPG = imread("Results_lab02/cross_0256x0256_025.jpg");
	BGRSplitAndMerge(image, imageJPG);

	waitKey(0);
	return(0);
}
