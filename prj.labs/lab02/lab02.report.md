## Работа 2. Исследование каналов и JPEG-сжатия
автор: Лазарев М.О.	
дата: 02.03.2021

<!-- url: https://gitlab.com/lenker0/lazarev-m-o/-/tree/master/prj.labs/lab02 -->

### Задание
1. В качестве тестового использовать изображение data/cross_0256x0256.png
2. Сохранить тестовое изображение в формате JPEG с качеством 25%.
3. Используя cv::merge и cv::split сделать "мозаику" с визуализацией каналов для исходного тестового изображения и JPEG-версии тестового изображения
- левый верхний - трехканальное изображение
- левый нижний - монохромная (черно-зеленая) визуализация канала G
- правый верхний - монохромная (черно-красная) визуализация канала R
- правый нижний - монохромная (черно-синяя) визуализация канала B
4. Результы сохранить для вставки в отчет
5. Сделать мозаику из визуализации гистограммы для исходного тестового изображения и
JPEG-версии тестового изображения, сохранить для вставки в отчет.

### Результаты

![](cross_0256x0256_025.jpg)

Рис. 1. Тестовое изображение после сохранения в формате JPEG с качеством 25%

![](cross_0256x0256_png_channels.png)

Рис. 2. Визуализация каналов исходного тестового изображения

![](cross_0256x0256_jpg_channels.png)

Рис. 3. Визуализация каналов JPEG-версии тестового изображения

![](cross_0256x0256_hists.png)

Рис. 4. Визуализация гистограм исходного и JPEG-версии тестового изображения


### Текст программы

```cpp
#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>


using namespace std;
using namespace cv;

void compress(Mat img, int image_quality) {
	vector<int> compression_params;
	compression_params.push_back(IMWRITE_JPEG_QUALITY);
	compression_params.push_back(image_quality);
	imwrite("Results_lab02/cross_0256x0256_025.jpg", img, compression_params);
}

void hist(Mat image, Mat imgG, Mat imgG25) {

	Mat imgConv = imread("Results_lab02/cross_0256x0256.jpg");

	cvtColor(image, imgG, COLOR_BGR2GRAY);
	cvtColor(imgConv, imgG25, COLOR_BGR2GRAY);

	Mat hist(256, 512, CV_8U, 255);
	Mat hist25(256, 512, CV_8U, 255);

	vector<int> vect(256);
	for (int i = 0; i < 256; i++)
		for (int j = 0; j < 256; j++)
			vect[imgG.at<uchar>(j, i)]++;
	int max = *max_element(vect.begin(), vect.end());
	for (int i = 0; i < 256; i++)
	{
		int t = vect[i] * 255 / max;
		Point p1(i * 2, 255 - t);
		Point p2(i * 2 + 1, 255);
		rectangle(hist, p1, p2, 0);
	}

	vector<int>vect_25(256);
	for (int i = 0; i < 256; i++)
		for (int j = 0; j < 256; j++)
			vect_25[imgG25.at<uchar>(j, i)]++;

	max = *max_element(vect_25.begin(), vect_25.end());
	for (int i = 0; i < 256; i++)
	{
		int t = vect_25[i] * 255 / max;
		Point p1(i * 2, 255 - t);
		Point p2(i * 2 + 1, 255);
		rectangle(hist25, p1, p2, 0);
	}

	Mat result(256, 256, CV_32F);
	hconcat(hist, hist25, result);
	imwrite("Results_lab02/cross_0256x0256_hists.png", result);
}

void BGRSplitAndMerge(Mat image, Mat imageJPG) {
	Mat imgB, imgG, imgR, imgB25, imgG25, imgR25;
	vector<Mat> channels;
	vector<Mat> ch(3);
	split(image, ch);
	Mat tmp = Mat::zeros(Size(image.cols, image.rows), CV_8UC1);

	channels.push_back(ch[0]);
	channels.push_back(tmp);
	channels.push_back(tmp);
	merge(channels, imgB);

	channels.clear();
	channels.push_back(tmp);
	channels.push_back(ch[1]);
	channels.push_back(tmp);
	merge(channels, imgG);

	channels.clear();
	channels.push_back(tmp);
	channels.push_back(tmp);
	channels.push_back(ch[2]);
	merge(channels, imgR);

	/*Clear channels and split for JPG img*/
	ch.clear();
	split(imageJPG, ch);
	/*--*/

	channels.clear();
	channels.push_back(ch[0]);
	channels.push_back(tmp);
	channels.push_back(tmp);
	merge(channels, imgB25);

	channels.clear();
	channels.push_back(tmp);
	channels.push_back(ch[1]);
	channels.push_back(tmp);
	merge(channels, imgG25);

	channels.clear();
	channels.push_back(tmp);
	channels.push_back(tmp);
	channels.push_back(ch[2]);
	merge(channels, imgR25);

	hconcat(image, imgR, image);
	hconcat(imgG, imgB, imgG);
	vconcat(image, imgG, image);

	hconcat(imageJPG, imgR25, imageJPG);
	hconcat(imgG25, imgB25, imgG25);
	vconcat(imageJPG, imgG25, imageJPG);

	imwrite("Results_lab02/cross_0256x0256_png_channels.png", image);
	imwrite("Results_lab02/cross_0256x0256_jpg_channels.png", imageJPG);

	hist(image, imgG, imgG25);
}

int main() {

	string path = "Resources/cross_0256x0256.png";
	Mat image = imread(path);
	Mat imageJPG;
	compress(image, 25);
	imageJPG = imread("Results_lab02/cross_0256x0256_025.jpg");
	BGRSplitAndMerge(image, imageJPG);

	waitKey(0);
	return(0);
}


```
