#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
#include <cmath>

using namespace std;
using namespace cv;

float y(float x){
	float an = pow((sin(x) + cos(2.14) / log(4)), 2);	
	return(an);
}


vector <uchar> bright() {
	vector<uchar> lut(256);
	for (int i = 0; i < 256; ++i) {
		lut[i] = 255 * y(i / 255.0);
	}
	return lut;
}


Mat graph(int w, int d) {
	Mat res(w, d, CV_8U, 255);
	for (int i = 0; i < d; ++i) {
		res.at<uchar>((w - 1) - (d - 1) * y(i / 511.0), i) = 0;
	}
	return res;
}


int main()
{
	Mat img, image_gre, image_gre_res, image_rgb_res;
	img = imread("Resources/cross_0256x0256.png");


	cvtColor(img, image_gre, COLOR_BGR2GRAY);
	vector<uchar> lut = bright();
	LUT(image_gre, lut, image_gre_res);
	LUT(img, lut, image_rgb_res);


	imwrite("Results_lab03/lab03_rgb.png", img);
	imwrite("Results_lab03/lab03_gre.png", image_gre);
	imwrite("Results_lab03/lab03_gre_res.png", image_gre_res);
	imwrite("Results_lab03/lab03_rgb_res.png", image_rgb_res);
	imwrite("Results_lab03/lab03_viz_func.png", graph(512, 512));

	waitKey(0);
	return 0;
}